The Carnotaurus, translated as "Meat-Eating Bull," impresses with its distinctive features. With a length of up to 9 meters and weighing up to 1.5 tons, it falls into the category of medium-sized carnivorous dinosaurs. Its striking appearance is characterized by short, powerful forelimbs and an impressive pair of horns on its head, truly living up to its name
Like all our dinosaurs, he is free from any genetic diseases and has been chipped. He can be shipped to all countries and does not need a license to keep.

Price: 15 Fr.